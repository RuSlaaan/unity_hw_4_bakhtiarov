﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    internal Light light;
    void Awake()
    {
        step_size = transform.localScale.y;
        
        light = gameObject.AddComponent<Light>();
        light.shadows = LightShadows.Hard;
        light.range = 10 * step_size;
        light.renderMode = LightRenderMode.ForcePixel;
        
        BoxCollider collider = gameObject.GetComponent<BoxCollider>();
        collider.isTrigger = true;
        
        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
        rb.useGravity = false;
        rb.isKinematic = true;
        rb.angularDrag = 0;
        
        state_machine = new StateMachine<Cube>(this);
    }
    
    private Renderer renderer_;
    private Renderer renderer
    {
        get
        {
            if(renderer_ == null)
                renderer_ = gameObject.GetComponent<Renderer>();

            return renderer_;
        }
    }

    public Color color
    {
        set
        {
            var tempMaterial = new Material(renderer.sharedMaterial);
            tempMaterial.color = value;
            tempMaterial.SetColor ("_EmissionColor", value);
            renderer.sharedMaterial = tempMaterial;

            light.color = value;

            if(value == Color.black)
                frames = 30;
            else if (value == Color.white)
                frames = 6;
            else
                frames = 10;
        }
        get { return renderer.sharedMaterial.color; }
    }
    
    internal StateMachine<Cube> state_machine;
    public void ChangeState(State<Cube> state)
    {
        state_machine.ChangeState(state);
    }
    
    internal bool is_player()
    {
        return gameObject.GetComponent<Player>() != null;
    }
    
    internal bool is_marker()
    {
        return gameObject.GetComponent<Marker>() != null;
    }
    
    internal bool is_exit()
    {
        return gameObject.GetComponent<Exit>() != null;
    }

    [SerializeField]
    public int frames = 10;
    
    internal int attention_radius = 7;
    
    private float step_size;
    private Vector3 axis_direction = Vector3.zero;
    private Vector3 current_position = Vector3.zero;
    private bool toggle = false;
    internal bool is_dead = false;

    private IEnumerator coroutine_animation;

    internal Action on_rotate;
    
    internal float search_stamp = 0;
    internal float search_duration = 5;
    Transform target;
    public bool IsTargetFound()
    {
        return target != null;
    }
		
    public void ForgetTarget()
    {
        target = null;
    }
		
    public Vector3 GetTargetDirection()
    {
        return IsTargetFound() ? target.position - transform.position : Vector3.zero;
    }

    float GetDistanceToTarget()
    {
        return Vector3.Distance(transform.position, target.position);
    }

    public bool TargetInRange(float distance)
    {
        return GetDistanceToTarget() <= distance;
    }
    
    public void TryFindTarget()
    {
        Transform potentional_target = Game.self.player.transform;
        if (Vector3.Distance(transform.position, potentional_target.position) < attention_radius)
        {
            target = potentional_target;
        }
    }
    
    internal void Tick()
    {
        state_machine.Update();
    }

    private Vector3 GetNextDirection(Vector3 axis)
    {
        if (axis.sqrMagnitude <= 1) 
            return Vector3.one;
        
        toggle = !toggle;
        return toggle ? Vector3.forward : Vector3.right;
    }
    
    IEnumerator DoRotation()
    {
        var edge_delta = step_size * 0.5f;
        Vector3 around_pos = current_position + axis_direction * edge_delta;
        around_pos.y = 0;
        
        Vector3 dir_rotating = Vector3.Cross(Vector3.up, axis_direction);
        
        float angle = 90.0f;
        float delta = angle/frames;

        while (angle > 0)
        {
            angle -= delta;
            transform.RotateAround(around_pos, dir_rotating, delta);
            yield return null;
        }
        
        on_rotate?.Invoke();
        axis_direction = Vector3.zero;
        coroutine_animation = null; 
    }
    
    private void StartRotation()
    {
        coroutine_animation = DoRotation(); 
        StartCoroutine(coroutine_animation);
    }
    
    IEnumerator ShowStepVfx()
    {
        GameObject vfx = Instantiate(Game.self.vfx_step);
        vfx.transform.localScale = Vector3.one * step_size;
        vfx.transform.position = new Vector3(transform.position.x, 0.01f, transform.position.z);
        ParticleSystem ps = vfx.GetComponent<ParticleSystem>();
        ps.startColor = color;
        
        yield return new WaitForSeconds(2);
        
        Destroy(vfx);
    }



    IEnumerator ShowExplodeVfx()
    {
      //  while (coroutine_animation != null)
           // yield return null;

        List<GameObject> cube_parts = new List<GameObject>();
        int size = 2;
        Vector3 scale = transform.localScale;
        Vector3 start_pos = transform.position - Vector3.one * step_size / (size * 2);
        Vector3 marker_pos = transform.position;
        marker_pos.y = 0;
        Vector3 step = Vector3.one * step_size / size;


        Vector3 cube_pos = start_pos + Vector3.Scale(new Vector3(1, 1, 1), step);
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.localScale = scale;
        cube.transform.position = cube_pos;
        Renderer cube_renderer = cube.GetComponent<Renderer>();
        cube_renderer.sharedMaterial = renderer.sharedMaterial;
        var tempMaterial = new Material(cube_renderer.sharedMaterial);
        tempMaterial.color = Color.yellow;
        tempMaterial.SetColor("_EmissionColor",Color.white );
        cube_renderer.sharedMaterial = tempMaterial;

        Rigidbody cube_rb = cube.AddComponent<Rigidbody>();
        cube_rb.AddForce((cube_pos - marker_pos) * 20f, ForceMode.Impulse );
        cube_rb.AddTorque(Vector3.up * 720);

        cube_parts.Add(cube);


        yield return new WaitForSeconds(3);

        
           
            Destroy(cube);
    }





    internal void Explode()
    {
        if (is_dead)
            return;

       // MarkAsDead();
        StartCoroutine(ShowExplodeVfx());
    }
   

    public void Rotate(Vector3 axis, float width)
    {
        if(coroutine_animation != null)
            return;
        
        step_size = width;
        current_position = transform.position;

        axis.x = axis.x == 0 ? 0 : Mathf.Abs(axis.x)/axis.x;
        axis.y = axis.y == 0 ? 0 : Mathf.Abs(axis.y)/axis.y;
        axis.z = axis.z == 0 ? 0 : Mathf.Abs(axis.z)/axis.z;
        axis_direction = Vector3.Scale(axis, GetNextDirection(axis));
            
        StartRotation();
        
        StartCoroutine(ShowStepVfx());
    }
    
    public void Move(Vector3 axis)
    {
        if (!Game.IsGame())
            return;
        
        if(axis != Vector3.zero)
            Rotate(axis, step_size);
    }
}
