﻿using UnityEngine;

[RequireComponent(typeof(Cube))]
public class Exit : MonoBehaviour
{
    internal Cube cube;
    void Awake()
    {
        cube = GetComponent<Cube>();
    }

    Color exit_color = Color.white;
    
    [SerializeField]
    public float speed_rotation = 50;
    public bool PlayerIsReadyForExit()
    {
        return Game.self.player.cube.color.Equals(exit_color);
    }

    private void Start()
    {
        cube.ChangeState(ExitDeactivatedState.instance);
    }

    void Update()
    {
        cube.Tick();
    }

    public void Init(Color color)
    {
        exit_color = color;
        cube.color = color;
    }
    
    void OnTriggerEnter(Collider other)
    {
        var other_cube = other.gameObject.GetComponent<Cube>();
        if (other_cube == null)
            return;
        
        if(other_cube.is_player() && PlayerIsReadyForExit())
            Game.GameOver(true);
    }
}
