﻿using UnityEngine;

public class StateMachine<T>
{
	public State<T> currentState { get; private set; }
	public T agent;

	public StateMachine(T _o)
	{
		agent = _o;
		currentState = null;
	}

	public void ChangeState(State<T> _newstate)
	{
		if(currentState != null)
			currentState.ExitState(agent);
		currentState = _newstate;
		currentState.EnterState(agent);
	}

	public void Update()
	{
		if (currentState != null)
			currentState.UpdateState(agent);
	}
}

public abstract class State<T>
{
	public abstract void EnterState(T _owner);
	public abstract void ExitState(T _owner);
	public abstract void UpdateState(T _owner);
}

public class DummyState : State<Cube>
{
	private static DummyState _instance;

	private DummyState()
	{
		if(_instance == null)
			_instance = this;
	}

	public static DummyState instance
	{
		get
		{
			if(_instance == null)
				new DummyState();

			return _instance;
		}
	}
	
	public override void EnterState(Cube agent){}
	public override void ExitState(Cube agent){}
	public override void UpdateState(Cube agent){}
}

public class PlayerInputState : State<Cube>
{
	private static PlayerInputState _instance;

	private PlayerInputState()
	{
		if(_instance == null)
			_instance = this;
	}

	public static PlayerInputState instance
	{
		get
		{
			if(_instance == null)
				new PlayerInputState();

			return _instance;
		}
	}
	
	public override void EnterState(Cube agent){}
	public override void ExitState(Cube agent){}
	public override void UpdateState(Cube agent)
	{
		agent.Move(Game.self.input.axis);
	}
}

public class PatrolState : State<Cube>
{
	private static PatrolState _instance;

	private PatrolState()
	{
		if(_instance == null)
			_instance = this;
	}

	public static PatrolState instance
	{
		get
		{
			if(_instance == null)
				new PatrolState();

			return _instance;
		}
	}

	public override void EnterState(Cube agent)
	{
	}

	public override void ExitState(Cube agent)
	{
	}

	public override void UpdateState(Cube agent)
	{
		if(!agent.is_marker())
			return;
			
		agent.Move(agent.GetComponent<Marker>().speed.normalized);

		if(!agent.IsTargetFound())
			agent.TryFindTarget();
		else
		{
			if(agent.GetComponent<Marker>().is_stalker())
				agent.ChangeState(PredatorState.instance);
			else if(agent.GetComponent<Marker>().is_walker())
				agent.ChangeState(VictimState.instance);
		}
	}
}

public class PredatorState : State<Cube>
{
	private static PredatorState _instance;

	private PredatorState()
	{
		if(_instance == null)
			_instance = this;
	}

	public static PredatorState instance
	{
		get
		{
			if(_instance == null)
				new PredatorState();

			return _instance;
		}
	}
	
	public override void EnterState(Cube agent)
	{
	}

	public override void ExitState(Cube agent)
	{
	}

	public override void UpdateState(Cube agent)
	{
		if (agent.is_marker() && agent.GetComponent<Marker>().is_stalker())
		{
			agent.GetComponent<Marker>().chase_speed = agent.GetTargetDirection();
			agent.Move(agent.GetComponent<Marker>().chase_speed.normalized);
			
			if (!agent.TargetInRange(agent.attention_radius))
				agent.ChangeState(SearchState.instance);
		}
	}
}

public class VictimState : State<Cube>
{
	private static VictimState _instance;

	private VictimState()
	{
		if(_instance == null)
			_instance = this;
	}

	public static VictimState instance
	{
		get
		{
			if(_instance == null)
				new VictimState();

			return _instance;
		}
	}
	
	public override void EnterState(Cube agent)
	{
	}

	public override void ExitState(Cube agent)
	{
	}

	public override void UpdateState(Cube agent)
	{
		if (agent.is_marker() && agent.GetComponent<Marker>().is_walker())
		{
			agent.GetComponent<Marker>().chase_speed = agent.GetTargetDirection() * -1;
			agent.Move(agent.GetComponent<Marker>().chase_speed.normalized);
			
			if (!agent.TargetInRange(agent.attention_radius))
				agent.ChangeState(SearchState.instance);
		}
	}
}

public class SearchState : State<Cube>
{
	private static SearchState _instance;

	private SearchState()
	{
		if(_instance != null)
			return;

		_instance = this;
	}

	public static SearchState instance
	{
		get
		{
			if(_instance == null)
				new SearchState();

			return _instance;
		}
	}
	
	public override void EnterState(Cube agent)
	{
		agent.search_stamp = Time.time;
	}

	public override void ExitState(Cube agent)
	{

	}

	public override void UpdateState(Cube agent)
	{
		if (agent.is_marker() && (agent.GetComponent<Marker>().is_stalker() || agent.GetComponent<Marker>().is_walker() ))
		{
			agent.Move(agent.GetComponent<Marker>().chase_speed.normalized);

			if (!agent.IsTargetFound())
				agent.TryFindTarget();
			else if (agent.TargetInRange(agent.attention_radius))
			{
				if(agent.GetComponent<Marker>().is_stalker())
					agent.ChangeState(PredatorState.instance);
				else if(agent.GetComponent<Marker>().is_walker())
					agent.ChangeState(VictimState.instance);
			}
				
			if (agent.search_stamp + agent.search_duration <= Time.time)
			{
				agent.ForgetTarget();
				agent.ChangeState(PatrolState.instance);
			}
		}
	}
}

public class ExitActivatedState : State<Cube>
{
	private static ExitActivatedState _instance;

	private ExitActivatedState()
	{
		if(_instance == null)
			_instance = this;
	}

	public static ExitActivatedState instance
	{
		get
		{
			if(_instance == null)
				new ExitActivatedState();

			return _instance;
		}
	}

	public override void EnterState(Cube agent)
	{
		agent.transform.rotation = Quaternion.identity;
	}
	public override void ExitState(Cube agent){}
	public override void UpdateState(Cube agent)
	{
		if (!agent.is_exit())
			return;
		
		Exit marker = agent.GetComponent<Exit>();

		if(!marker.PlayerIsReadyForExit())
			agent.ChangeState(ExitDeactivatedState.instance);
	}
}

public class ExitDeactivatedState : State<Cube>
{
	private static ExitDeactivatedState _instance;

	private ExitDeactivatedState()
	{
		if(_instance == null)
			_instance = this;
	}

	public static ExitDeactivatedState instance
	{
		get
		{
			if(_instance == null)
				new ExitDeactivatedState();

			return _instance;
		}
	}
	
	public override void EnterState(Cube agent){}
	public override void ExitState(Cube agent){}
	public override void UpdateState(Cube agent)
	{
		if (!agent.is_exit())
			return;

		Exit marker = agent.GetComponent<Exit>();

		if(!marker.PlayerIsReadyForExit())
			marker.transform.Rotate(0.0f, Time.deltaTime * marker.speed_rotation, 0.0f);
		else
			agent.ChangeState(ExitActivatedState.instance);
			
	}
}