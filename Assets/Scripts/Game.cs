﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    private static GameState state;
    enum GameState
    {
        GAME,
        VICTORY,
        DEFEAT
    }

    public static Game self;

    public GameObject player_go;
    public GameObject exit_go;
    public GameObject exit_pulse_go;
    public GameObject marker_go;
    public GameObject arrow_go;
    public GameObject camera_go;
    public GameObject vfx_step;
    public AudioClip sfx_step;
    public AudioClip ambient;
    public Canvas canvas;
    public Light Mylight;
    Image time_go;
    const float exit_time_max = 20;
    float exit_time = 0;

    float spawn_markers_stamp;
    float spawn_markers_cooldown = 0.5f;

    GameObject player_listener;
    Light lig;
    internal PlayerInput input;
    internal Exit exit;
    internal Player player;
    internal Color exit_color;
    Marker first_marker;
    Animator time_animator;
    AudioSource ambient_source;
    
    PlayerFollowing player_following;

    void Awake()
    {
        self = this;
    }

    void Start()
    {
        state = GameState.GAME;
        marker_go.SetActive(false);
        
        exit_color = GetRandomColor(7);

        first_marker = CreateMarker();
        first_marker.Init(exit_color, first_marker.transform.position, Vector3.zero, this);

        player_listener = new GameObject();
        player_listener.AddComponent<AudioListener>();

        GameObject base_go = canvas.transform.Find("base").gameObject;
        GameObject stick_go = base_go.transform.Find("stick").gameObject;
        input = new PlayerInput();
        input.Init(base_go, stick_go);

        player = player_go.AddComponent<Player>();
        //    player.cube.color = exit_color;

        exit = exit_go.AddComponent<Exit>();
        exit.Init(exit_color);

        player_following = camera_go.AddComponent<PlayerFollowing>();
        player_following.Init(player.gameObject);

        GameObject time_child = arrow_go.transform.Find("time").gameObject;
        time_go = time_child.GetComponent<Image>();
        time_go.fillAmount = 1;

        time_animator = time_child.GetComponent<Animator>();
        time_animator.SetBool("Alert", false);
        time_animator.SetTrigger("Cancel");

        Color pulse_color = exit_color;
        pulse_color.a = 0.25f;
        ColorizeGo(exit_pulse_go, pulse_color);

        ambient_source = gameObject.AddComponent<AudioSource>();
        ambient_source.clip = ambient;
        ambient_source.loop = true;
        ambient_source.Play();

        exit_time = exit_time_max;
    }

    void ColorizeGo(GameObject go, Color color)
    {
        Renderer renderer = go.GetComponent<Renderer>();
        var tempMaterial = new Material(renderer.sharedMaterial);
        tempMaterial.color = color;
        tempMaterial.SetColor("_EmissionColor", Mathf.LinearToGammaSpace(0.02f) * color);
        renderer.sharedMaterial = tempMaterial;
    }

    float GetLeftTime()
    {
        return exit_time;
    }

    Marker CreateMarker()
    {
        GameObject marker = Instantiate(marker_go);
        marker.SetActive(true);
        return marker.AddComponent<Marker>();
    }

    float exit_arrow_radius = 20;
    float spawn_radius = 30;
    Rect spawn_rect = Rect.zero;
    void Update()
    {
        if (!IsGame())
        {
            arrow_go.SetActive(false);
            exit_pulse_go.SetActive(false);
            ambient_source.volume = 0;
            return;
        }

        if (GetLeftTime() == 0)
            GameOver(false);
        else
            time_animator.SetBool("Alert", GetLeftTime() < 5);

        input.Tick();

        player_listener.transform.position = player.transform.position;
        ambient_source.volume = player.activity * 0.2f;

        Vector3 player_pos = player.transform.position;
        Vector3 exit_pos = exit.transform.position;

        exit_pulse_go.SetActive(exit.PlayerIsReadyForExit());

        float delta = 45;
        Vector3 player_screen_pos = Camera.main.WorldToScreenPoint(player_pos);
        Vector3 exit_screen_pos = Camera.main.WorldToScreenPoint(exit_pos);
        player_screen_pos.z = 0;
        exit_screen_pos.z = 0;

        arrow_go.transform.position = new Vector3(Mathf.Clamp(exit_screen_pos.x, delta, Screen.width - delta), Mathf.Clamp(exit_screen_pos.y, delta, Screen.height - delta), 0);
        arrow_go.transform.Find("open").right = (exit_screen_pos - arrow_go.transform.position).normalized;
        arrow_go.transform.Find("open").gameObject.SetActive(exit.PlayerIsReadyForExit());
        arrow_go.transform.Find("locked").gameObject.SetActive(!exit.PlayerIsReadyForExit());
        arrow_go.transform.Find("color").GetComponent<RawImage>().color = exit_color;

        if (first_marker != null)
            return;

        exit_time = Mathf.Max(0, exit_time - Time.deltaTime);
        time_go.fillAmount = exit_time / exit_time_max;

        if (spawn_markers_stamp + spawn_markers_cooldown > Time.time)
            return;

        spawn_markers_stamp = Time.time;
        spawn_rect.position = new Vector2(player_pos.x - spawn_radius, player_pos.z - spawn_radius);
        spawn_rect.size = new Vector2(spawn_radius * 2, spawn_radius * 2);

        Vector3 start_pos;
        Vector3 direction;
        float random_pos = 2 * spawn_radius * Random.value;

        switch (Random.Range(0, 4))
        {
            case 0:
                start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.back * random_pos;
                direction = Vector3.right;
                break;
            case 1:
                start_pos = new Vector3(spawn_rect.xMax, 0, spawn_rect.yMax) + Vector3.back * random_pos;
                direction = Vector3.left;
                break;
            case 2:
                start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.right * random_pos;
                direction = Vector3.back;
                break;
            default:
                start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMin) + Vector3.right * random_pos;
                direction = Vector3.forward;
                break;
        }
        Color color = GetRandomColor();
        Marker random_marker = CreateMarker();
        random_marker.Init(color, start_pos, direction, this);
    }
   public static Color newcolor2 = new Color(0.3f, 0.3f, 0.5f, 0f);
   public static Color newcolor = new Color(0.7f, 0.5f, 0.8f, 0f);
    private List<Color> colors = new List<Color> { Color.white,newcolor2, Color.black,newcolor, Color.yellow, Color.magenta, Color.cyan, Color.red, Color.green, Color.blue };
    private Color GetRandomColor(int start_idx = 0)
    {
        return colors[Random.Range(start_idx, colors.Count)];
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.green;
        Gizmos.DrawCube(new Vector3(spawn_rect.center.x, 0, spawn_rect.center.y), new Vector3(spawn_rect.width, 0, spawn_rect.height));
    }

    void OnGUI() {
        if (!IsGame())
        {
            GUI.BeginGroup(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100));
            GUI.Box(new Rect(0, 0, 100, 100), state == GameState.VICTORY ? "VICTORY" : "DEFEAT");
            if (GUI.Button(new Rect(10, 40, 80, 30), "Play again!"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            GUI.EndGroup();
        }
    }
    public void lightt (){
        Mylight.enabled =! Mylight.enabled;
    }
  public static bool IsGame()
  {
    return state == GameState.GAME;
  }
  
  public static void GameOver(bool is_victory)
  {
    state = is_victory ? GameState.VICTORY : GameState.DEFEAT;
  }
   
  internal void IncreaseTime(int seconds)
  {
    exit_time = Mathf.Min(exit_time_max, exit_time + seconds);
    time_animator.SetTrigger("Add");
  }  
}
