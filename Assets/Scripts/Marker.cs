﻿using UnityEngine;

[RequireComponent(typeof(Cube))]
public class Marker : MonoBehaviour
{
    public Color newcolor = new Color(0.7f, 0.5f, 0.8f, 0f);
    public Color newcolor2 = new Color(0.3f, 0.3f, 0.5f, 0f);
    internal Cube cube;
  

    void Awake()
    {
        cube = GetComponent<Cube>();
        
        cube.on_rotate = delegate
        {
            step_source.Play();
        };
    }

    private float max_volume = 0.3f;
    private AudioSource step_source;
    internal Vector3 speed;
    internal Vector3 chase_speed;
    private Game game;

    private bool is_destroyer()
    {
        return cube.color == Color.black || is_stalker();
    }
    
    private bool is_reverser()
    {
        return cube.color == Color.cyan;
    }
    
    internal bool is_stalker()
    {
        return cube.color == Color.magenta;
    }
    
    internal bool is_walker()
    {
        return cube.color == Game.self.exit_color;
    }
    
    public void Init(Color color, Vector3 position, Vector3 speed, Game game)
    {
        this.game = game;
        cube.color = color;
        transform.position = position;
        this.speed = speed;
        
        step_source = gameObject.AddComponent<AudioSource>();
        step_source.clip = Game.self.sfx_step;
        step_source.volume = max_volume;
        step_source.pitch = cube.color == Color.black ? 0.1f : 50;
        step_source.rolloffMode = AudioRolloffMode.Linear;
        step_source.minDistance = 1;
        step_source.maxDistance = 10;
        step_source.spatialBlend = 1;
        
        cube.ChangeState(PatrolState.instance);
    }
    
    void Update()
    {
        if (time > 0)
        {
         //   time = Mathf.Max(0, time - Time.deltaTime);// что-то не работает((:
        }
        else
        {

          //  transform.localScale = new Vector3(1f, 1f, 1f);
          //  minimum = 1f;
        }
        cube.Tick();
       
       

    }

    void OnTriggerEnter(Collider other)
    {
        var other_cube = other.gameObject.GetComponent<Cube>();
        if (other_cube == null)
            return;

        if (is_destroyer())
        {
            if (other_cube.is_player())
                Game.GameOver(false);
            else if (other_cube.is_marker() && other_cube.color != Color.black)
                Destroy(other.gameObject);
        }
        else if (is_reverser())
        {
            var all_markers = FindObjectsOfType<Marker>();
            for (int i = 0; i < all_markers.Length; i++)
            {
                var marker = all_markers[i];
                marker.speed = marker.speed * -1;
            }
            other_cube.Explode();

            Destroy(gameObject);
        }
        else if (other_cube.is_player())
        {
            if (cube.color != Color.white && cube.color != Color.black && cube.color != Color.yellow && cube.color != newcolor && cube.color != newcolor2)
                other_cube.GetComponent<Player>().SwallowColor(cube.color);
            else if (cube.color == Color.white)
                game.IncreaseTime(10);
            else if (cube.color == Color.yellow)
                other_cube.GetComponent<Player>().Scale30();
            else if (cube.color == newcolor)
                game.lightt();
             else if(cube.color == newcolor2)
                Scalee();






            other_cube.Explode();

            Destroy(gameObject);
        } 
    }
    float minimum = 1f;

    public float time ;
    void Scalee()
    {
     
    float maximum = (minimum / 100f * 30f);
        Vector3 scale = new Vector3(maximum, maximum, maximum);
        var all_markers = FindObjectsOfType<Marker>();
      
        for (int i = 0; i < all_markers.Length; i++)
        {
            var marker = all_markers[i];
            marker.transform.localScale += scale;
        }
        minimum = minimum + maximum;

        time = 10f;

    }


    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
