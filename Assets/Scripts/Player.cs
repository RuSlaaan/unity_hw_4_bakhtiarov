﻿using UnityEngine;

[RequireComponent(typeof(Cube))]
public class Player : MonoBehaviour
{
    internal Cube cube;
    const float exit_time_max = 5;
    float exit_time = 0;
    void Awake()
    {
        cube = GetComponent<Cube>();
        cube.on_rotate = delegate
        {
            step_source.Play();
        };
    }
    
    private AudioSource step_source;
    
    internal float activity;
    float activity_step = 0.01f;
    void Start()
    {
        step_source = gameObject.AddComponent<AudioSource>();
        step_source.clip = Game.self.sfx_step;
        step_source.volume = 0.3f;
        
        cube.ChangeState(PlayerInputState.instance);
       
    }
    
    public void Update()
    {
        activity = Mathf.Clamp01(activity + (Game.self.input.axis.magnitude > 0.1f ? activity_step : activity_step * -1));
        cube.Tick();

        if (exit_time > 0)
        {
            exit_time = Mathf.Max(0, exit_time - Time.deltaTime);
        }
        else {
            transform.localScale = new Vector3(2f,2f,2f);
            minimum = 2f;
        }
    }

    public void SwallowColor(Color color)
    {
        Color new_color = cube.color - Color.white*0.2f + color * 0.6f;
        new_color.r = Mathf.Clamp01(new_color.r);
        new_color.g = Mathf.Clamp01(new_color.g);
        new_color.b = Mathf.Clamp01(new_color.b);
        new_color.a = 1;
        cube.color = new_color;
    }

    public static float minimum = 2f;
    public void Scale30()
    {
      
        float maximum = (minimum / 100f * 30f) ;
        Vector3 scale = new Vector3(maximum, maximum, maximum);
        transform.localScale += scale;
        minimum = minimum + maximum;
       
        exit_time = exit_time_max;

    }

    


}
